package com.gost_group.rosseti_wso2;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAspectJAutoProxy
public class RossetiApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(RossetiApplication.class, args);
    }
}
