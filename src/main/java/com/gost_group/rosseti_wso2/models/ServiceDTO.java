package com.gost_group.rosseti_wso2.models;

/**
 * Created by M. Anatoly on 19.01.2017.
 */
public class ServiceDTO {

    private String name;

    private String description;

    private Boolean isActive;

    private String type;

    private String upTime;

    private String endpoint;

    private String wsdl11;

    private String wsdl12;

    public ServiceDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getWsdl11() {
        return wsdl11;
    }

    public void setWsdl11(String wsdl11) {
        this.wsdl11 = wsdl11;
    }

    public String getWsdl12() {
        return wsdl12;
    }

    public void setWsdl12(String wsdl12) {
        this.wsdl12 = wsdl12;
    }
}
