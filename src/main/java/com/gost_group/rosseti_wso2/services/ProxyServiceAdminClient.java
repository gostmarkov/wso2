package com.gost_group.rosseti_wso2.services;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.wso2.carbon.proxyadmin.stub.ProxyServiceAdminProxyAdminException;
import org.wso2.carbon.proxyadmin.stub.ProxyServiceAdminStub;
import org.wso2.carbon.proxyadmin.stub.types.carbon.ProxyData;

import java.rmi.RemoteException;

/**
 * Created by M. Anatoly on 20.01.2017.
 */
public class ProxyServiceAdminClient {
    private final String serviceName = "ProxyServiceAdmin";
    private ProxyServiceAdminStub proxyServiceAdminStub;
    private String endPoint;

    public ProxyServiceAdminClient(String backEndUrl, String sessionCookie) throws AxisFault {
        this.endPoint = backEndUrl + "/services/" + serviceName;
        proxyServiceAdminStub = new ProxyServiceAdminStub(endPoint);

        ServiceClient serviceClient;
        Options option;
        serviceClient = proxyServiceAdminStub._getServiceClient();
        option = serviceClient.getOptions();
        option.setManageSession(true);
        option.setProperty(org.apache.axis2.transport.http.HTTPConstants.COOKIE_STRING, sessionCookie);
    }

    public ProxyServiceAdminStub getProxyServiceAdminStub(){
        return proxyServiceAdminStub;
    }

    public void deleteProxy(String name) throws ProxyServiceAdminProxyAdminException, RemoteException {
        proxyServiceAdminStub.deleteProxyService(name);
    }

    public void editDescription(String name, String description) throws ProxyServiceAdminProxyAdminException, RemoteException {
        ProxyData proxyData = proxyServiceAdminStub.getProxy(name);
        proxyData.setDescription(description);
        proxyServiceAdminStub.modifyProxy(proxyData);
    }
}
