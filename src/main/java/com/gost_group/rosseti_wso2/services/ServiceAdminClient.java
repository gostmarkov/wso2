package com.gost_group.rosseti_wso2.services; /**
 * Created by M. Anatoly on 13.01.2017.
 */

import com.gost_group.rosseti_wso2.models.ServiceDTO;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.wso2.carbon.service.mgt.stub.ServiceAdminException;
import org.wso2.carbon.service.mgt.stub.ServiceAdminServerException;
import org.wso2.carbon.service.mgt.stub.ServiceAdminStub;
import org.wso2.carbon.service.mgt.stub.types.carbon.ServiceMetaData;
import org.wso2.carbon.service.mgt.stub.types.carbon.ServiceMetaDataWrapper;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class ServiceAdminClient {
    private final String serviceName = "ServiceAdmin";
    private ServiceAdminStub serviceAdminStub;
    private String endPoint;

    public ServiceAdminClient(String backEndUrl, String sessionCookie) throws AxisFault {
        this.endPoint = backEndUrl + "/services/" + serviceName;
        serviceAdminStub = new ServiceAdminStub(endPoint);
        //Authenticate Your stub from sessionCooke
        ServiceClient serviceClient;
        Options option;
        serviceClient = serviceAdminStub._getServiceClient();
        option = serviceClient.getOptions();
        option.setManageSession(true);
        option.setProperty(org.apache.axis2.transport.http.HTTPConstants.COOKIE_STRING, sessionCookie);
    }

    public void deleteService(String[] serviceGroup) throws RemoteException {
        serviceAdminStub.deleteServiceGroups(serviceGroup);
    }

    public void startService(String name) throws RemoteException, ServiceAdminException {
        serviceAdminStub.startService(name);
    }

    public void deactiveService(String name) throws RemoteException, ServiceAdminException {
        serviceAdminStub.stopService(name);
    }

    public void editDescription(String descr){
    }

    public List<ServiceDTO> getAll() throws RemoteException, ServiceAdminServerException, ServiceAdminException {
        List<ServiceDTO> services = new ArrayList<>();
        for(ServiceMetaData serviceMetaData:serviceAdminStub.listServices("ALL", "*", 0).getServices()){
            ServiceDTO serviceDTO = new ServiceDTO();
            serviceDTO.setName(serviceMetaData.getName());
            serviceDTO.setType(serviceMetaData.getServiceType());
            serviceDTO.setDescription(serviceAdminStub.getServiceData(serviceMetaData.getName()).getDescription());
            serviceDTO.setActive(serviceMetaData.getActive());
            services.add(serviceDTO);
        }
        return services;
    }

    public ServiceAdminStub getServiceAdminStub(){
        return  serviceAdminStub;
    }

    public ServiceMetaDataWrapper listServices() throws RemoteException {
        return serviceAdminStub.listServices("ALL", "*", 0);
    }
}
