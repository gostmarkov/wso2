package com.gost_group.rosseti_wso2.controllers;

import com.gost_group.rosseti_wso2.models.ServiceDTO;
import com.gost_group.rosseti_wso2.services.LoginAdminServiceClient;
import com.gost_group.rosseti_wso2.services.ProxyServiceAdminClient;
import com.gost_group.rosseti_wso2.services.ServiceAdminClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.wso2.carbon.authenticator.stub.LoginAuthenticationExceptionException;
import org.wso2.carbon.authenticator.stub.LogoutAuthenticationExceptionException;
import org.wso2.carbon.service.mgt.stub.ServiceAdminException;
import org.wso2.carbon.service.mgt.stub.ServiceAdminServerException;


import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by M. Anatoly on 16.01.2017.
 */
@RestController
@RequestMapping(value = "admin")
public class AdminController {

    @RequestMapping(value = "service/deactive/{serviceName}")
    public void deactiveService(@PathVariable String serviceName) throws Exception {
        System.setProperty("javax.net.ssl.trustStore", "/home/wso2/wso2esb-5.0.0/repository/resources/security/10.8.0.46.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "pastol");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        String backEndUrl = "https://10.8.0.46:9446";

        LoginAdminServiceClient login = new LoginAdminServiceClient(backEndUrl);
        String session = login.authenticate("admin", "admin");
        ServiceAdminClient serviceAdminClient = new ServiceAdminClient(backEndUrl, session);
        //ServiceMetaDataWrapper serviceList = serviceAdminClient.listServices();
        serviceAdminClient.deactiveService(serviceName);

        login.logOut();
    }

    @RequestMapping(value = "service/start/{serviceName}")
    public void startService(@PathVariable String serviceName) throws Exception {
        System.setProperty("javax.net.ssl.trustStore", "/home/wso2/wso2esb-5.0.0/repository/resources/security/10.8.0.46.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "pastol");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        String backEndUrl = "https://10.8.0.46:9446";

        LoginAdminServiceClient login = new LoginAdminServiceClient(backEndUrl);
        String session = login.authenticate("admin", "admin");
        ServiceAdminClient serviceAdminClient = new ServiceAdminClient(backEndUrl, session);
        //ServiceMetaDataWrapper serviceList = serviceAdminClient.listServices();
        serviceAdminClient.startService(serviceName);

        login.logOut();
    }

    @RequestMapping(value = "service/delete/{serviceName}")
    public void deleteProxy(@PathVariable String serviceName) throws Exception {
        System.setProperty("javax.net.ssl.trustStore", "/home/wso2/wso2esb-5.0.0/repository/resources/security/10.8.0.46.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "pastol");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        String backEndUrl = "https://10.8.0.46:9446";

        LoginAdminServiceClient login = new LoginAdminServiceClient(backEndUrl);
        String session = login.authenticate("admin", "admin");
        ProxyServiceAdminClient proxyServiceAdminClient = new ProxyServiceAdminClient(backEndUrl, session);
        //ServiceMetaDataWrapper serviceList = serviceAdminClient.listServices();
        proxyServiceAdminClient.deleteProxy(serviceName);

        login.logOut();
    }

    @RequestMapping(value = "service/editDescription/{serviceName}",
            method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void editProxyDescription(@PathVariable String serviceName,
                                     @RequestParam(name = "description") String description) throws Exception {
        System.setProperty("javax.net.ssl.trustStore", "/home/wso2/wso2esb-5.0.0/repository/resources/security/10.8.0.46.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "pastol");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        String backEndUrl = "https://10.8.0.46:9446";

        System.out.println(serviceName);
        System.out.println(description);
        LoginAdminServiceClient login = new LoginAdminServiceClient(backEndUrl);
        String session = login.authenticate("admin", "admin");
        ProxyServiceAdminClient proxyServiceAdminClient = new ProxyServiceAdminClient(backEndUrl, session);
        //ServiceMetaDataWrapper serviceList = serviceAdminClient.listServices();
        proxyServiceAdminClient.editDescription(serviceName, description);

        login.logOut();
    }

    @RequestMapping(value = "service/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ServiceDTO> getAll() throws RemoteException, LoginAuthenticationExceptionException, ServiceAdminException, ServiceAdminServerException, LogoutAuthenticationExceptionException {
        System.setProperty("javax.net.ssl.trustStore", "/home/wso2/wso2esb-5.0.0/repository/resources/security/10.8.0.46.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "pastol");
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        String backEndUrl = "https://10.8.0.46:9446";

        LoginAdminServiceClient login = new LoginAdminServiceClient(backEndUrl);
        String session = login.authenticate("admin", "admin");
        ServiceAdminClient serviceAdminClient = new ServiceAdminClient(backEndUrl, session);
        List<ServiceDTO> services = serviceAdminClient.getAll();
        login.logOut();
        return services;
    }


}
