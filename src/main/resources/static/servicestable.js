$(document).ready(function(){


    table = $("#servicestable").DataTable({

        "autoWidth":false,
        "ajax":{
            "url":"admin/service/all",
            "dataSrc":""
        },
        "columns":[
            {"data":"name"},
            {
                "orderable":false,
                "defaultContent":"",
                "render":renderDescr
            },
            {
                "orderable":false,
                "defaultContent":"",
                "render":renderDescrBtn
            },
            {"data":"type"},
            {"data":"active"},
            {
                "orderable":false,
                "defaultContent":"",
                "render":renderWsdlAdress
            },
            {
                "orderable":false,
                "defaultContent":"",
                "render":renderEditBtn
            },
            {
                "orderable":false,
                "defaultContent":"",
                "render":renderDeleteBtn
            }
        ]
        ,
        "initComplete": function(){
            makeEditable();
        }

    });


    /*table.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            console.log(that);
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );

        $( 'select', this.footer() ).on( 'change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );

    } );

    $("#newstable_filter").find("label").html("Поиск по содержанию: <input type='search' class='form-control input-sm' placeholder='' aria-controls='newstable'>")
    $("#newstable_filter").find("input").on('keyup change', function(){
        var content_cal=table.column(6);
        if (content_cal.search()!==this.value){
            content_cal
                .search( this.value )
                .draw();
        }
    })*/




});