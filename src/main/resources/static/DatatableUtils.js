/**
 * Created by M. Anatoly on 19.01.2017.
 */
var form;
function makeEditable(){
}

function add() {
    clearForm();
    $('#editRow').modal();
}

function updateRow(name, active){
    var action = "start";
    if (active == true) {
        action = "deactive";
    }
    $.ajax({
        "url":"admin/service/"+action+"/"+name,
        "type":"GET",
        "success":function(data){
            table.ajax.reload();
        }
    });
}

function editDescription(name){
    console.log(name);
    var description = $("textarea[pr_name='"+name+"']").val();
    console.log(description);
    $.ajax({
        "url":"admin/service/editDescription/"+name,
        "type":"POST",
        "dataType": 'json',
        "data":{
          "description": description
        },
        "success":function(){
            window.location.reload();
        }
    })
}



function deleteRow(name){
    $.ajax({
        "url":"admin/service/delete/"+name,
        "type":"GET",
        "success":function(){
            window.location.reload();
        }
    })
}

function renderEditBtn(data, type, row) {

    if (type == 'display') {
        var btnName = "выкл";
        if (row.active==false){
            btnName = "вкл ";
        }
        return '<a class="btn btn-xs btn-primary" onclick="updateRow(\'' + row.name+'\', '+ row.active + ');">'+btnName+'</a>';
    }
    return data;
}

function renderDeleteBtn(data, type, row) {
    if (type == 'display') {
        if(row.type=='proxy') {
            return '<a class="btn btn-xs btn-danger" onclick="deleteRow(\'' + row.name + '\');">Удалить</a>';
        } else {
            return null;
        }
    }
    return data;
}

function renderDescr(data, type, row) {
    if (type == 'display') {
        return '<textarea pr_name="'+row.name+'">' + row.description + '</textarea>';
    }
    return data;
}

function renderDescrBtn(data, type, row) {
    if (type == 'display') {
        if(row.type=='proxy'){
            return '<a class="btn btn-xs btn-primary" onclick="editDescription(\'' + row.name +'\');">Редактировать</a>';
        } else {
            return null
        }
    }
    return data;
}

function renderWsdlAdress(data, type, row) {
    if (type == 'display') {
        if (row.active==true){
            return '<a href="http://52739.simplecloud.club:8283/services/'+ row.name +'?wsdl">WSDL</a>';
        } else {
            return null;
        }
    }
    return data;
}


function clearForm(){
    form.find("input, textarea").val("");
    $('#id').val(0);
    form.find("option:selected").each(function(ind, opt){
        $(opt).prop("selected", false);
    })
}